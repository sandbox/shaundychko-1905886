<?php

/**
 * @file
 * ckeditor_adaptive_images settings UI
 */

/**
 * Form builder; Choose which image styles are available on the CKEditor image
 * insert dialog.
 * @ingroup forms
 * @see system_settings_form()
 */
function ckeditor_adaptive_images_admin_settings() {
  $form = array();
  $default_suffix = variable_get('resp_img_default_suffix', NULL);
  $styles = image_styles();
  $adaptive_styles = array();

  foreach ($styles as $key => $value) {
    // if this style has the default resp_img suffix, then
    // put the style name in this variable with the suffix
    // removed.
    $style_without_suffix = strstr($key, $default_suffix, TRUE);
    if ($style_without_suffix) {
      $adaptive_styles[] = $style_without_suffix;
    }
  }

  // make a form with a checkbox and weight beside each resp_img
  // enabled image style

  $form['description'] = array(
    '#type' => 'item',
    '#title' => t('Choose which resp_img styles will be available in the CKEditor image dialog. Only image styles with the resp_img default suffix will be displayed here.'),
  );

  // create a hidden form element to hold adaptive image styles so we know what
  // to look for in the submit handler
  $form['resp_img_styles'] = array(
    '#type' => 'hidden',
    '#value' => $adaptive_styles,
  );
  // retrieve pre-exisiting settings
  $ckeditor_adaptive_images_styles = variable_get(
    'ckeditor_adaptive_images_styles', array());

  // loop through each adaptive image style and place a checkbox and weight.
  foreach ($adaptive_styles as $style_name) {
    $form[$style_name] = array(
      '#type' => 'fieldset',
      '#title' => t('\'@name\' image style', array('@name' => $style_name)),
    );
    $form[$style_name][$style_name . '_checkbox'] = array(
      '#type' => 'checkbox',
      '#default_value' => isset($ckeditor_adaptive_images_styles[$style_name]) ?
        $ckeditor_adaptive_images_styles[$style_name]['enabled'] : 0,
      '#title' => t('Include \'@name\' image style in the CKEditor image dialog',
        array('@name' => $style_name)),
    );
    $form[$style_name][$style_name . '_weight'] = array(
      '#type' => 'select',
      '#title' => t('Weight'),
      '#options' => array(
        '1' => t('1'),
        '2' => t('2'),
        '3' => t('3'),
        '4' => t('4'),
        '5' => t('5'),
        '6' => t('6'),
        '7' => t('7'),
        '8' => t('8'),
        '9' => t('9'),
        '10' => t('10'),
      ),
      '#default_value' => isset($ckeditor_adaptive_images_styles[$style_name]) ?
        $ckeditor_adaptive_images_styles[$style_name]['weight'] : 1,
      '#description' => t('This controls the sort order of image styles in the CKEditor \'width\' drop-down. Higher weights sink to the bottom of the list.'),
    );
  } 
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save',
  );
  return $form;
}

/**
 * Submit handler for the admin UI. Places chosen image styles in the variables
 * table and generates a custom config file for CKEditor.
 */
function ckeditor_adaptive_images_admin_settings_submit($form, &$form_state) {
  $adaptive_image_styles = $form_state['values']['resp_img_styles'];
  $ckeditor_adaptive_images_styles = array();
  $default_suffix = variable_get('resp_img_default_suffix', NULL);
  foreach ($adaptive_image_styles as $image_style) {
    $ckeditor_adaptive_images_styles[$image_style]['enabled'] =
      $form_state['values'][$image_style . '_checkbox'];
    $ckeditor_adaptive_images_styles[$image_style]['weight'] = 
      $form_state['values'][$image_style . '_weight'];
  }

  uasort($ckeditor_adaptive_images_styles, 'compare_weights');
  variable_set('ckeditor_adaptive_images_styles', $ckeditor_adaptive_images_styles);
  drupal_set_message(t('Your settings have been saved'));

  // generate the custom CKEditor config file
  ckeditor_adaptive_images_generate_config($ckeditor_adaptive_images_styles,
    $default_suffix);
}

/**
 * Helper function to sort image styles by weight
 */
function compare_weights($a, $b) {
  if ($a['weight'] == $b['weight']) {
    return 0;
  }
  return ($a['weight'] < $b['weight']) ? -1 : 1;
}

/**
 * Generate the custom CKEditor config file
 */
function ckeditor_adaptive_images_generate_config($styles, $default_suffix) {
  // styles to include in ckeditor
  $ckeditor_styles = array();
  foreach ($styles as $key => $value) {
    if ($value['enabled'] == 1) {
      $ckeditor_styles[] = $key;
    }
  }
  // build the JS array that will be inserted into the CKEditor config file
  $js_array = '';
  foreach ($ckeditor_styles as $style) {
    $js_array .= '[ \'' . $style . '\'],';
  }
  $js_array = '[ ' . $js_array . ' [\'not_set\'] ], \'default\': \'not_set\',';
  // create the file using heredoc syntax. Is there a better way of doing this?
  $data = <<<JS
CKEDITOR.editorConfig = function( config )
{
  // this disables the browser resize handles since the width will now be set 
  // in the image dialog. This also prevents CKEditor from automatically adding
  // pesky inline width and height styles. Resize handles are also removed from tables.
  config.disableObjectResizing = true;

};

// When opening a dialog, a 'definition' is created for it. For
// each editor instance the 'dialogDefinition' event is then
// fired. We can use this event to make customizations to the
// definition of existing dialogs.
CKEDITOR.on( 'dialogDefinition', function( event ) {
  // Take the dialog name
  var dialogName = event.data.name;
  // The definition holds the structured data that is used to eventually
  // build the dialog and we can use it to customize just about anything.
  // In Drupal terms, it's sort of like CKEditor's version of a Forms API and
  // what we're doing here is a bit like a hook_form_alter.
  var dialogDefinition = event.data.definition;


  // Resources for the following:
  // Download: https://github.com/ckeditor/ckeditor-dev
  // see /plugins/image/dialogs/image.js
  // and refer to http://docs.ckeditor.com/#!/api/CKEDITOR.dialog.definition
  // visit: file:///[path_to_ckeditor-dev]/plugins/devtools/samples/devtools.html
  // for an excellent way to find machine names for dialog elements
  if (dialogName == 'image' ) {
    dialogDefinition.removeContents( 'advanced' );
    dialogDefinition.removeContents( 'Link' );
    var infoTab = dialogDefinition.getContents( 'info' );
    var altText = infoTab.get( 'txtAlt' );
    var IMAGE = 1,
        LINK = 2,
        PREVIEW = 4,
        CLEANUP = 8;
    // updatePreview is copied from ckeditor image plugin
    var updatePreview = function( dialog ) {
      //Don't load before onShow.
      if ( !dialog.originalElement || !dialog.preview )
        return 1;

      // Read attributes and update imagePreview;
      dialog.commitContent( PREVIEW, dialog.preview );
      return 0;
    };
    // add the select list for choosing the image width.
    infoTab.add( {
      type: 'select',
      id: 'imageSize',
      label: 'Image Width (required)',
      items: $js_array
      onChange: function() {
        // this = CKEDITOR.ui.dialog.select
        var dialog = this.getDialog();
        var element = dialog.originalElement;
        element.setAttribute( 'data-image-size', this.getValue() );
        updatePreview( this.getDialog() );
      },
      setup: function( type, element ) {
        if ( type == IMAGE ) {
          var value = element.getAttribute( 'data-image-size' );
          this.setValue( value );
        }
      },
      // created a custom data-image-size attribute since working with classes
      // might step on the toes of anything else adding classes to images?
      // this is also easier since there's no need to remove previously
      // applied classes.
      commit: function( type, element ) {
        if (type == IMAGE ) {
          if ( this.getValue() || this.isChanged() ) {
            element.setAttribute( 'data-image-size', this.getValue() );
          }
          console.log('data-image-size: ' + this.getValue());
        } else if ( type == PREVIEW ) {
          element.setAttribute( 'data-image-size', this.getValue() );
        } else if ( type == CLEANUP ) {
          element.setAttribute( 'data-image-size', '' );
        }
      },
      validate: function() {
        if ( this.getValue() == 'not_set' ) {
          alert('Please choose an image width');
          return false;
        } else {
          return true;
        }
      }
    },
      //position before preview
      'htmlPreview'
    );

    // Put a title attribute field on the main 'info' tab
    infoTab.add( {
      type: 'text',
      id: 'txtTitle',
      label: 'The title attribute is used as a tooltip when the mouse hovers over the image.',
      onChange: function() {
        updatePreview( this.getDialog() );
      },
      setup: function( type, element ) {
        if ( type == IMAGE )
          this.setValue( element.getAttribute( 'title' ) );
      },
      commit: function( type, element ) {
        if ( type == IMAGE ) {
          if ( this.getValue() || this.isChanged() )
            element.setAttribute( 'title', this.getValue() );
        } else if ( type == PREVIEW ) {
          element.setAttribute( 'title', this.getValue() );
        } else if ( type == CLEANUP ) {
          element.removeAttribute( 'title' );
        }
      }
    },
      // position before the imageSize select box
      //'imageSize'
      'htmlPreview'
    );

    // make CKEditor insert a publically accessible adaptive version of all images
    // this requires http://drupal.org/project/ais
    var srcField = infoTab.get( 'txtUrl' );
    srcField.setup = function( type, element ) {
      if ( type == IMAGE ) {
        var url = element.getAttribute( 'data-src-original' ) ||
element.getAttribute( 'src' );
        var field = this;
        this.getDialog().dontResetSize = true;
        field.setValue( url );
        field.setInitValue();
      }
    };
    // override the commit function of the 'txtUrl' field
    srcField.commit = function( type, element ) {
      var srcOriginal = this.getValue();
      var regex_modify = /^(.*sites\/[^\/]+\/files\/)(.+)$/;
      var srcFinal = '';
      var parent_dialog = this.getDialog();
      var width_field = parent_dialog.getContentElement( 'info' , 'imageSize' );
      var the_width = width_field.getValue();
      var default_suffix = '$default_suffix';
      // modify path only if adaptive image style is not already applied.
      if (the_width != 'not_set') {
        srcFinal = srcOriginal.replace( regex_modify,
          '$1styles/' + the_width + default_suffix + '/public/$2');
      } else {
        srcFinal = srcOriginal;
      }
      if ( type == IMAGE && ( this.getValue() || this.isChanged() ) ) {
        element.data( 'cke-saved-src', srcFinal );
        element.setAttribute( 'data-src-original', srcOriginal);
        element.setAttribute( 'src', srcFinal );
      } else if ( type == CLEANUP ) {
        element.setAttribute( 'data-src-original', '' );
        element.setAttribute( 'src', '' ); // If removeAttribute doesn't work.
        element.removeAttribute( 'src' );
      }
    };

    // add a select widget to choose image alignment
    infoTab.add( {
      type: 'select',
      id: 'imageAlign',
      label: 'Image Alignment',
      items: [ [ 'Not Set', '' ], [ 'Left', 'left'],
               [ 'Right', 'right' ], [ 'Center', 'center'] ],
      'default': '',
      onChange: function() {
        // this = CKEDITOR.ui.dialog.select
        updatePreview( this.getDialog() );
      },
      setup: function( type, element ) {
        if ( type == IMAGE ) {
          var value = element.getAttribute( 'data-image-align' );
          this.setValue( value );
        }
      },
      // created a custom data-image-align attribute since working with classes
      // might step on the toes of anything else adding classes to images?
      commit: function( type, element ) {
        if (type == IMAGE ) {
          if ( this.getValue() || this.isChanged() ) {
            element.setAttribute( 'data-image-align', this.getValue() );
          }
        } else if ( type == PREVIEW ) {
          element.setAttribute( 'data-image-align', this.getValue() );
        } else if ( type == CLEANUP ) {
          element.setAttribute( 'data-image-align', '' );
        }
      }

    },
      //position before imageSize
      'imageSize'
    );

    // Improve the alt field label. Copied from Drupal's image field.
    altText.label = 'The alt attribute may be used by search engines, ' +
      'and screen readers.';

    // Remove a bunch of extraneous fields. These properties will be set in
    // the theme or module CSS.
    infoFieldsRemove = [ 'cmbAlign', 'txtWidth', 'txtHeight', 'ratioLock', 'txtBorder',
                     'txtHSpace', 'txtVSpace' ];
    for( i = 0; i < infoFieldsRemove.length; i++ ) {
      infoTab.remove( infoFieldsRemove[i] );
    }
  }
});
JS;
  $filename = file_unmanaged_save_data($data, 'public://ckeditor_adaptive_images_config.js',
    FILE_EXISTS_REPLACE);
  if ($filename) {
    drupal_set_message(t('The configuration file
      ckeditor_adaptive_images_config.js has been saved to the default files
      directory.'));
  }
  else {
    drupal_set_message(t('The configuration file could not be saved.'), 'error');
  }
}
