This project was motivated by two goals:

1) Make the entire inline WYSIWYG image insertion happen in one dialog. This means the image alignment, size, choice of file... etc. should all be consolidated in one place. Also, get rid of unnecessary stuff on the dialog.
2) Images should be 'adaptive' for responsive web design

Dependencies:
WYSIWYG
Responsive images and styles 7.x-1.x branch
IMCE (not technically required, but I'm not sure what you'd do without it)
IMCE WYSIWYG Bridge

Approach taken by this module

The CKEditor image dialog is modified to make is simpler, and give a select box to choose an image style.

The 'link' tab is removed since editors can use the regular 'link' button to make images clickable, and the 'advanced' tab is removed since all that stuff seems extraneous, with the exception of the 'title' attribute field.

The 'title' attribute field is added to the main dialog, along with a select box for image width and alignment. Width and Alignment each add 'data' attributes ('data-image-width' and 'data-image-align' to the img tag and the module comes with a basic .css file with rules to get started. The Alignment box has a 'center' option added.

A custom configuration file is generated for CKEditor containing the image styles that have the resp_img default suffix which you selected at admin/config/media/ckeditor_adaptive_images. The CKEditor image drop-down will be populated with the chosen styles (displayed without the suffix).

The URL of each image file is re-written by inserting "/styles/[image_style_name_with_default_suffix]/public" into the path: 'sites/[site-name or default]/files/...[insert image style string here] .../the_rest_of_the_regular_path_to_file'. This triggers the Responsive Images and Styles module to do it's work with delivering different file sizes to different width devices.

Browser resize handles are removed from images (and tables also, as it turns out) in order to avoid pesky inline height and width styles, and also because width is being selected in the image dialog.

How to install

Install the dependencies as per usual
If you're using an admin theme to edit content, the CSS from this module won't apply in the WYSIWYG. You'll need to define your own CSS files by editing the WYSIWYG profile at /admin/config/content/wysiwyg/profile... Either add this module's CSS file: '%b/sites/all/modules/ckeditor_adaptive_images/ckeditor_adaptive_images.css' for example, or include it's rules in one of the files you've included already.
You need to visit and submit the admin page at admin/config/media/ckeditor_adaptive_images in order to generate the configuration file that's sent to CKEditor.
Make sure you've enabled the resp_img module for the textarea by visiting the field configuration.
This was developed and tested with CKEditor 4.0.1.x. The 7.x-2.x branch of Responsive Images and Styles doesn't yet support inline images, so this was made against the 7.x-1.x branch.

Know Limitations

Images in the WYSIWYG edit screen are not responsive. This probably isn't a problem. However, if the resp_img default suffix is for a mobile targeted style, then images might look grainy while being edited on a desktop size browser since the file size will be what's meant for mobile. This won't be a problem on the regular view.
This works only for public images
If you're using or testing this module, let me know what you think! Code reviews and feedback are welcome.

